"""
Responsible for representing the user's chequing account.
"""
import account

class ChequingAccount(account.Account):
    def __init__(self, acctNum, name, rate, balance, overdraft=1000):
        super().__init__(acctNum, name, rate, balance)
        self._overdraftAllowed = overdraft

    def withdraw(self, amt):
        gotoOverdraft = self.getCurrentBalance() - amt
        if gotoOverdraft < 0 and self._overdraftAllowed <= 1000:
            overdraft = amt - self.getCurrentBalance
            self._overdraftAllowed -= overdraft
        elif gotoOverdraft > 1000:
            print("Sorry, you cannot withdraw more than $1000 in overdraft.")
            canWithdraw = self.getCurrentBalance() - amt
            print(f"Currently, the max you can withdraw is {canWithdraw}")
        else:
            self.withdraw(amt)