"""
Responsible for creating blue-print of a user's account with the bank
"""
import transaction

class Account:
    def __init__(self, acctNum, name, rate, balance):
        """
        Default constructor representing an account of our user
        @params
            acctNum: int
            name: str
            rate: float
            balance: float
        """
        self._accountNumber = acctNum
        self._accountHolderName = name
        self._rateOfInterest = rate
        self._currentBalance = balance
        self.listTransactions = []

    def getAccountNumber(self):
        """
        Method that retrieves the account number
        """
        return self._accountNumber

    def getAccountHolderName(self):
        """
        Method that retrieves the name of the account holder
        """
        return self._accountHolderName

    def getRateOfInterest(self):
        """
        Method that retrieves the rate of interest specified for the account
        """
        return self._rateOfInterest

    def getCurrentBalance(self):
        """
        Method that retrieves the current balance of the user
        """
        return self._currentBalance

    def deposit(self, funds):
        """
        Mutator that adds funds to the user's account
        """
        self._currentBalance += funds
        return self._currentBalance

    def withdraw(self, funds):
        """
        Mutator that decreases funds from the user's account
        """
        self._currentBalance -= funds
        return self._currentBalance

    def addTransactions(self, transaction):
        """
        Method that appends the transactions occurring in an account to a list
        """
        self.listTransactions.append(transaction) # --> transaction would contain instances of class Transaction