"""
Responsible for mediating the interaction between the user and the Bank 
"""
import bank
import transaction

class Program():
    """
    Responsible for interacting with the user and implementing logic
    """
    def run(self):
        """
        Class method that will initiate the program when instantiated
        """
        self.showMainMenu()

    def showMainMenu(self):
        """
        Displays banking main menu until the user chooses to exit the application
        
        choice: integer that represents user's selection in main menu
        """
        while True:
            try:
                choice = int(input("""Please select an option from the following -
                    \n(1) Open account
                    \n(2) Select account
                    \n(3) Exit
                    \nYour selection: """))
                if choice == 1:
                    print("You chose to open an account")
                elif choice == 2:
                    while True:
                        try:
                            self.showAccountMenu()
                            userContinue = input("Do you want to perform another search? ")
                            userContinue.lower()
                            if userContinue == "no" or userContinue == "n":
                                exit()
                        except TypeError:
                            print('Invalid entry')
                elif choice == '':
                    break
                else:
                    exit()
            except ValueError as valer:
                print(valer)
                continue
            else:
                break

    def showAccountMenu(self):
        """
        Allows user to enter account number of the account they want to work with
        """
        finder = bank.Bank()
        userAccount = int(input("Please enter the acct number (5 characters *****): "))
        searchUser = finder.searchAccount(userAccount)
        
        while True:
            try:
                choice = int(input("""\nPlease make a selection - (1) Check balance, (2) Deposit, (3) Withdraw, (4) Display Transactions, (5) Exit Account
                    \nYour selection here: """))
                if choice == 1:
                    print(f"\nYour balance is ${searchUser.getCurrentBalance()}")
                elif choice == 2:
                    funds = int(input("\nHow much would you like to deposit? "))
                    searchUser.deposit(funds)
                    print(f"You deposited ${funds}")
                    deposit = transaction.Transaction("Deposit",funds)
                    searchUser.addTransactions(deposit)
                    print(f"Your current balance is now ${searchUser.getCurrentBalance()}\n")
                elif choice == 3:
                    while True:
                        funds = int(input("How much would you like to withdraw? "))
                        if funds > searchUser.getCurrentBalance():
                            print("Sorry, you don't have enough funds.")
                            continue
                        else:
                            searchUser.withdraw(funds)
                            print(f"\nYou withdrew ${funds}")
                            withdraw = transaction.Transaction("Withdraw",funds)
                            searchUser.addTransactions(withdraw) # adds successful withdrawal to the list of transactions
                            break
                        print(f"\nYour current balance is now ${searchUser.getCurrentBalance()}\n")
                elif choice == 4:
                    print("\nHere are a list of your transactions: ")
                    for i in range(len(searchUser.listTransactions)):
                        print(f"{searchUser.listTransactions[i].getTransactionType()}: ${searchUser.listTransactions[i].getTransactionAmount()}")
                elif choice == '':
                    break
                else:
                    print("Goodbye...")
                    exit()
                # try/except handling here
            except ValueError:
                print('Sorry, invalid entry.')
                continue
            else:
                break