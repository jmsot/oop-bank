"""
Responsible for listing out the user's transactions
"""

class Transaction():
    def __init__(self, transType, amount):
        self._transactionType = transType
        self._transactionAmount = amount
    
    def getTransactionType(self):
        return self._transactionType

    def getTransactionAmount(self):
        return self._transactionAmount