"""
Responsible for implementing business logic required for the banking
"""
import account

class Bank:
    """
    Class that keeps track of all accounts and defines a list of account objects
    """
    account_IDS = [
    [12345, "John Smith", 0.02, 670.00],
    [67890, "Jane Goodall", 0.025, 90000.00],
    [13579, "John Snow", 0.015, 345.50],
    [24860, "Katy Perry", 0.025, 120000.00],
    [14570, "Adam Sandler", 0.030, 80000.00]
    ]

    def accountList(self):
        """
        Class method for creating new accounts by fetching list information
        """
        self.users = []
        for i in range(len(Bank.account_IDS)):
            user = Bank.parse(self.account_IDS[i])
            self.users.append(user)
        return self.users

    @staticmethod
    def parse(userItems):
        """
        Uses the list information and returns a new user record created using the list items
        """
        user = account.Account(userItems[0],userItems[1],userItems[2],userItems[3])
        return user
    
    def searchAccount(self, userAccount):
        """
        Method that searches for a match from the list of hard-coded values and returns an object of the user account
        @params:
            userAccount: int of a 5-character input from the user
        returns:
            self.userID: obj instantiated in class Account from module account
        """
        for i in range(len(Bank.account_IDS)):
            if userAccount == Bank.account_IDS[i][0]:
                self.userID = self.accountList()[i]
        return self.userID