"""
Responsible for representing the user's savings account.
"""
import account

class SavingsAccount(account.Account):
    def __init__(self, sav_balance):
        self._minimumBalance = 1000
        self._balance = sav_balance

    def withdraw(self, sav_amount):
        newBalance = self._balance - sav_amount
        if newBalance < self._minimumBalance:
            print(f"You need to keep at least ${self._minimumBalance} in your savings account.")
            newAmount = sav_amount - self._minimumBalance
            print(f"You can take out ${newAmount}.")
            self._balance -= newAmount
        else:
            self._balance = newBalance
        return self._balance